package com.example.projectbird

data class Chat(
    val message: String,
    val dateTime: String
)