package com.example.projectbird.network

import android.net.Uri


/**
 * Created by Endra on 2020-01-19.
 */
open class Api {
    private val BASE_URL = "http://192.168.100.7/"

    fun userLogin(): String {
        return Uri.parse(BASE_URL).buildUpon()
            .appendPath("api")
            .appendPath("auth")
            .appendPath("login")
            .build()
            .toString()
    }

    fun userRegister(): String {
        return Uri.parse(BASE_URL).buildUpon()
            .appendPath("api")
            .appendPath("auth")
            .appendPath("register")
            .build()
            .toString()
    }
}