package com.example.projectbird.LoginRegister

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.projectbird.MenuUtamaActivity
import com.example.projectbird.R
import com.example.projectbird.network.Api
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_login.setOnClickListener {

            val emailLogin: String = edEmailLogin.text.toString()
            val passwordLogin: String = edPasswordLogin.text.toString()

            if(emailLogin.isEmpty()){
                Toast.makeText(this, "Silahkan isi email", Toast.LENGTH_SHORT).show()
            }else if (passwordLogin.isEmpty()){
                Toast.makeText(this, "Silahkann isi password", Toast.LENGTH_SHORT).show()
            }else{
<<<<<<< HEAD
                loginUser()
                val i = Intent(this, MenuUtamaActivity::class.java)
                startActivity(i)
=======
                sendingUserAndPassword(emailLogin,passwordLogin)
>>>>>>> 709545b3323fac100a008c03ccc61a4ec31748da
            }

        }

    }

<<<<<<< HEAD
    private fun loginUser() {

        val jsonobj = JSONObject()

        jsonobj.put("email",edEmailLogin.text)
        jsonobj.put("password",edPasswordLogin.text)

        val que = Volley.newRequestQueue(this)
        val request = JsonObjectRequest(Request.Method.POST, url,jsonobj,
            Response.Listener {
                    response ->
                Toast.makeText(this,"berhasil", Toast.LENGTH_SHORT).show()

            }, Response.ErrorListener {
                    error -> Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
            })
=======
    private fun sendingUserAndPassword(emailLogin: String, passwordLogin: String) {
        val que = Volley.newRequestQueue(this)
        val params = HashMap<String, String>()
        params.put("email",emailLogin)
        params.put("password",passwordLogin)

        val jsonObj = JSONObject(params as Map<*, *>)

        val url = Api().userLogin();
        val request = JsonObjectRequest(Request.Method.POST, url,jsonObj, Response.Listener { response ->
            try {
                processResponse(response)
            }catch (e:Exception){
                Log.e("testException",e.toString())
            }
        }, Response.ErrorListener {

        })

        request.retryPolicy = DefaultRetryPolicy(
            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
            // 0 means no retry
            0, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        // Add the volley post request to the request queue
>>>>>>> 709545b3323fac100a008c03ccc61a4ec31748da
            que.add(request)
    }

    private fun processResponse(response: JSONObject?) {
        try {
            val status = response!!.getString("status")
            val message = response!!.getString("message")
            if (status.equals("1")){
                Toast.makeText(this,"Ready To save",Toast.LENGTH_LONG).show()
            }else{
                Toast.makeText(this,message,Toast.LENGTH_LONG).show()
            }
        }catch (e:Exception){

        }
    }

    fun tambah(view: View){
        val i = Intent(this, RegisterActivity::class.java)
        startActivity(i)
    }
}
