package com.example.projectbird.LoginRegister

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.projectbird.MenuUtamaActivity
import com.example.projectbird.R
import com.example.projectbird.Info.UsersInfo
import com.example.projectbird.Users
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        btn_register.setOnClickListener {
            register()
        }


    }

    private fun register() {
        val nama: String = edNama.text.toString()
        val email: String = edEmail.text.toString()
        val password: String = edPwd.text.toString()
        val rePassword: String = edRePwd.text.toString()
        val noHp: String = edNohp.text.toString()

        if (nama.isEmpty()) {
            Toast.makeText(this, "Silahkan isi nama", Toast.LENGTH_SHORT).show()
        } else if (email.isEmpty()) {
            Toast.makeText(this, "Silahkan isi email", Toast.LENGTH_SHORT).show()
        } else if (password.isEmpty()) {
            Toast.makeText(this, "Silahkan isi password", Toast.LENGTH_SHORT).show()
        } else if (rePassword.isEmpty()) {
            Toast.makeText(this, "Silahkan ulangi password", Toast.LENGTH_SHORT).show()
        } else if (noHp.isEmpty()) {
            Toast.makeText(this, "Silahkan isi nomor handphone", Toast.LENGTH_SHORT).show()
        }else if (password != rePassword){
            Toast.makeText(this, "Password harus sama", Toast.LENGTH_SHORT).show()
        }
        else {
            readyToSave()
        }

    }

    private fun readyToSave() {


        val textView = findViewById<TextView>(R.id.edNama)


        // Instantiate the RequestQueue.
        val queue = Volley.newRequestQueue(this)
        val url = "192.168.100.7/api/auth/login"

        // Request a string response from the provided URL.
        val stringRequest = StringRequest(
            Request.Method.POST, url,
            Response.Listener<String> { response ->
                // Display the first 500 characters of the response string.
                textView.text = "Berhasil menampilkan data"
            },
            Response.ErrorListener { textView.text = "That didn't work!" })

        // Add the request to the RequestQueue.
        queue.add(stringRequest)



    }

//    private fun register() {
//
//        try {
//
//            Fuel.post(RegisterURL, listOf("name" to  etname!!.text.toString()
//                , "hobby" to  ethobby!!.text.toString()
//                , "username" to  etusername!!.text.toString()
//                , "password" to  etpassword!!.text.toString()
//            )).responseJson { request, response, result ->
//                Log.d("plzzzzz", result.get().content)
//                onTaskCompleted(result.get().content,RegTask)
//            }
//        } catch (e: Exception) {
//
//        } finally {
//
//        }
//
//    }

}



