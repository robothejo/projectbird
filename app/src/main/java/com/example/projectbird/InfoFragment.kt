package com.example.projectbird

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.projectbird.Info.AdapterInfo
import com.example.projectbird.Info.UsersInfo
import kotlinx.android.synthetic.main.fragment_info.*


@Suppress("UNREACHABLE_CODE")
class InfoFragment : Fragment() {

    val list = ArrayList<UsersInfo>()
    val listUsers = arrayOf(
        "Google",
        "Apple",
        "Microsoft",
        "Asus",
        "Zenpone",
        "Acer"
    )

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: AdapterInfo

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_info, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        linearLayoutManager =  LinearLayoutManager(context)
        recycler_view_info.layoutManager = linearLayoutManager
        adapter = AdapterInfo(list)
        recycler_view_info.adapter = adapter

        for (i in 0 until listUsers.size) {
            list.add(UsersInfo(listUsers.get(i)))
        }

        adapter.notifyDataSetChanged()
    }
}