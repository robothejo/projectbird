package com.example.projectbird

import com.example.projectbird.models.BlogPost

class DataSource {
    companion object{

        fun createDataSet(): ArrayList<BlogPost>{
            val list = ArrayList<BlogPost>()
            list.add(
                BlogPost(
                    "Walpaper pertama",
                    "You made it to the end of the course!\r\n\r\nNext we'll be building the REST API!",
                    "https://firebasestorage.googleapis.com/v0/b/projectbird-bb072.appspot.com/o/gnome_1.jpg?alt=media&token=95025c60-4c8c-496e-917b-9c0b3f583ef5",
                    "Sally"
                )
            )
            list.add(
                BlogPost(
                    "Walpaper kedua",
                    "The REST API course is complete. You can find the videos here: https://codingwithmitch.com/courses/build-a-rest-api/.",
                    "https://firebasestorage.googleapis.com/v0/b/projectbird-bb072.appspot.com/o/gnome_2.png?alt=media&token=bafd6fa6-6065-444f-af7a-b412d6c4de8a",
                    "mitch"
                )
            )


            list.add(
                BlogPost(
                    "Walpaper ketiga",
                    "Kaushik Gopal is a Senior Android Engineer working in Silicon Valley.\r\n\r\nHe works as a Senior Staff engineer at Instacart.\r\n\r\nInstacart: https://www.instacart.com/",
                    "https://raw.githubusercontent.com/mitchtabian/Kotlin-RecyclerView-Example/json-data-source/app/src/main/res/drawable/senior_android_engineer_kaushik_gopal.png",
                    "mitch"
                )
            )
            return list
        }
    }
}
