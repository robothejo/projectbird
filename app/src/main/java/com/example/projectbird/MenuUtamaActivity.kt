package com.example.projectbird

import android.content.ContentValues
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import kotlinx.android.synthetic.main.activity_menu_utama.*
import kotlinx.android.synthetic.main.list_item_report.*
import me.ibrahimsn.lib.OnItemSelectedListener


class MenuUtamaActivity : AppCompatActivity() {

    lateinit var chatFragment: ChatFragment
    lateinit var infoFragment: InfoFragment
    lateinit var profilFragment: ProfilFragment
    lateinit var reportFragment: ReportFragment

    private val PERMISSION_CODE = 1000
    private val CAMERA_REQUEST_CODE = 1
    var image_uri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_utama)

        chatFragment = ChatFragment()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frameLayout, chatFragment)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

        bottomBar.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelect(pos: Int) {
                when (pos) {
                    0 -> {
                        chatFragment = ChatFragment()
                        supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.frameLayout, chatFragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commit()
                    }

                    1 -> {
                        reportFragment = ReportFragment()
                        supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.frameLayout, reportFragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commit()
                    }
                    2 -> {
                        openCamera()

                    }

                    3 -> {
                        infoFragment = InfoFragment()
                        supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.frameLayout, infoFragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commit()
                    }
                    4 -> {
                        profilFragment = ProfilFragment()
                        supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.frameLayout, profilFragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commit()
                    }
                }
            }
        })

    }

    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        image_uri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        //camera intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE)
    }

//    private fun openCamera() {
//
//        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//        startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE)
//
//    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
//            val imageBitmap = data?.extras?.get("data") as Bitmap
//            blog_image.setImageBitmap(imageBitmap)
        }
    }


}