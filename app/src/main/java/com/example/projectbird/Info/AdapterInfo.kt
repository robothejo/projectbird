package com.example.projectbird.Info

import android.view.LayoutInflater
import kotlinx.android.synthetic.main.list_item_info.view.*
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.projectbird.R


class AdapterInfo(private val list:ArrayList<UsersInfo>) : RecyclerView.Adapter<AdapterInfo.Holder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_info,parent,false))
    }

    override fun getItemCount(): Int = list?.size

    override fun onBindViewHolder(holder: Holder, position: Int) {

        holder.view.txtBerita.text = list?.get(position)?.name

    }

    class Holder(val view: View) : RecyclerView.ViewHolder(view)

}